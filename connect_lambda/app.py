import json
import boto3
import os
from datetime import datetime
from http import HTTPStatus
from uuid import uuid4
from boto3.dynamodb.conditions import Key
from aws_lambda_powertools.tracing import Tracer
from aws_lambda_powertools.logging import correlation_paths, Logger
from aws_lambda_powertools.event_handler.api_gateway import APIGatewayRestResolver, CORSConfig, Response, content_types

AWS_REGION = os.getenv("AWS_REGION")

logger = Logger()
tracer = Tracer()


def __get_table_client():
    TABLE_NAME = "recare"
    AWS_REGION_DYNAMODB = "us-east-1"
    dynamodb = boto3.resource("dynamodb", region_name=AWS_REGION_DYNAMODB)
    table = dynamodb.Table(TABLE_NAME)
    return table


table = __get_table_client()

cors_config = CORSConfig(allow_origin="*")
app = APIGatewayRestResolver(cors=cors_config)


@app.exception_handler(ValueError)
def handle_value_error(ex: ValueError):
    metadata = {"path": app.current_event.path}
    logger.error(f"Malformed request: {ex}", extra=metadata)

    return Response(
        status_code=400,
        content_type=content_types.TEXT_PLAIN,
        body="Invalid request",
    )


@app.post("/connect/request/<user_id>")
def connect_request(user_id):
    data = app.current_event.json_body
    current_time = datetime.utcnow().isoformat()[:-3] + "Z"

    item = {
        "PK": f"USER#{user_id}",
        "SK": f"CONNECT_REQUEST#{uuid4()}",
        "request_from": data.get("request_from"),
        "request_to": user_id,
        "created_at": current_time
    }

    try:
        table.put_item(
            Item=item
        )
    except Exception:
        raise ValueError("Request body is not correctly formated")

    return Response(
        status_code=HTTPStatus.CREATED.value,
        content_type="application/json",
        body=HTTPStatus.CREATED.phrase
    )


@app.delete("/connect/request/<user_id>/<request_id>")
def delete_connect_request(user_id, request_id):
    try:
        table.delete_item(
            Key={
                "PK": f"USER#{user_id}",
                "SK": f"CONNECT_REQUEST#{request_id}",
            }
        )
    except Exception:
        raise ValueError("Something went wrong")

    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=HTTPStatus.OK.phrase
    )


def connect_users(user_from, user_to):
    current_time = datetime.utcnow().isoformat()[:-3] + "Z"

    item = {
        "PK": f"USER#{user_from}",
        "SK": f"CONNECT#{user_to}",
        "created_at": current_time
    }

    try:
        table.put_item(
            Item=item
        )
    except Exception:
        raise ValueError("Request body is not correctly formated")


@app.post("/connect/accept/<user_id>/<request_id>")
def accept_connect_request(user_id, request_id):
    data = app.current_event.json_body

    connect_users(data["request_from"], data["request_to"])
    connect_users(data["request_to"], data["request_from"])

    response = delete_connect_request(user_id, request_id)
    return response


@app.get("/connect/request/<user_id>")
def connect_request_list(user_id):
    response = table.query(
        KeyConditionExpression=Key("PK").eq(f"USER#{user_id}") & Key(
            "SK").begins_with("CONNECT_REQUEST")
    )
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=json.dumps(response["Items"])
    )


@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
def lambda_handler(event, context):
    # if(event["identity"] is None):
    #     return Response(
    #         status_code=HTTPStatus.UNAUTHORIZED.value,
    #         body=json.dumps({
    #             "status": HTTPStatus.UNAUTHORIZED.phrase,
    #             "message": HTTPStatus.UNAUTHORIZED.description
    #         }),
    #         content_type="application/json"
    #     )

    return app.resolve(event, context)
